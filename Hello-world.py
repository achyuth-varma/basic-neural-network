import tensorflow as tf
from tensorflow import keras
import numpy as np

model = keras.Sequential([keras.layers.Dense(units=1, input_shape=[1])])

model.compile(optimizer='sgd', loss='mean_squared_error')

xs = np.array([1., 2., 3., 4., 5.], dtype=float)
ys = np.array([4., 7., 10., 13., 16.], dtype=float)

model.fit(xs, ys, epochs=500)

print(model.predict([7.0]))